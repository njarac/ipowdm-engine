# -*- coding: utf-8 -*-
"""
Created on Mon Apr 25 15:40:47 2022

@author: redno
"""
from controller import Controller
from bitRate import BitRate
from network import Network
from event import Event
from event import EventType
# import simulator.source.uniformVariable as uniformVariable  # ???
from uniformVariable import UniformVariable
# import expVariable  # es el otro archivo o una libreria?
from expVariable import ExpVariable
import math
from datetime import datetime


class Simulator:
    _events = None
    _arriveVariable = None
    _departVariable = None
    _srcVariable = None
    _dstVariable = None
    _bitRateVariable = None
    _controller = None
    _currentEvent = None
    _bitRates = None
    _confidence = 0.0
    _zScore = 0.0
    _initReady = False
    _lambda = 0.0
    _mu = 0.0
    _seedArrive = 0
    _seedDeparture = 0
    _seedSrc = 0
    _seedDst = 0
    _seedBitRate = 0
    _numberOfEvents = 0
    _goalConnections = 0
    _nextEventTime = 0.0
    _allocationStatus = 0.0
    _rtnAllocation = 0.0
    _src = 0
    _dst = 0
    _bitRate = 0
    _bitRatesDefault = None
    _blockingProbability = 0.0
    _columnWidth = 0

    _startingTime = None
    _checkTime = None
    _timeDuration = None
    

    def __init__(self, networkFile="", pathFilename="", bitrateFilename="", timeOn = 1000, timeOff = 0.5):
        self.defaultValues()
        self._clock = 0
        self._controller = Controller()
        if (networkFile != ""):
            self._controller._network = Network(networkFile, timeOn, timeOff)
        if (pathFilename != ""):
            self._controller.setPaths(pathFilename, timeOn = timeOn, timeOff = timeOff)
        self._events = []
        if (bitrateFilename != ""):
            self._bitRatesDefault = BitRate().readBitRateFile(bitrateFilename)
        else:
            self._bitRatesDefault = []
            auxB = BitRate(10.0)
            auxB.addModulation("BPSK", 1, 5520)
            self._bitRatesDefault.append(auxB)
            auxB = BitRate(40.0)
            auxB.addModulation("BPSK", 4, 5520)
            self._bitRatesDefault.append(auxB)
            auxB = BitRate(100.0)
            auxB.addModulation("BPSK", 8, 5520)
            self._bitRatesDefault.append(auxB)
            auxB = BitRate(400.0)
            auxB.addModulation("BPSK", 32, 5520)
            self._bitRatesDefault.append(auxB)
            auxB = BitRate(1000.0)
            auxB.addModulation("BPSK", 80, 5520)
            self._bitRatesDefault.append(auxB)
        self.__lastConnectionIsAllocated = Controller.Status.N_A
        self.__agentAction = None
        self.__doneFunc = None

    def setLambda(self, lambdaValue):
        if (self._initReady):
            raise "You can not set lambda parameter AFTER calling init simulator method."
        self._lambda = lambdaValue

    def setMu(self, mu):
        if (self._initReady):
            raise "You can not set lambda parameter AFTER calling init simulator method."
        self._mu = mu

    def setSeedArrive(self, seed):
        if (self._initReady):
            raise "You can not set seed arrive parameter AFTER calling init simulator method."
        self._seedArrive = seed

    def setSeedDeparture(self, seed):
        if (self._initReady):
            raise "You can not set seed departure parameter AFTER calling init simulator method."
        self._seedArrive = seed

    def setSeedBitRate(self, seed):
        if (self._initReady):
            raise "You can not set seed bitrate parameter AFTER calling init simulator method."
        self._seedArrive = seed

    def setSeedSrc(self, seed):
        if (self._initReady):
            raise "You can not set seed source parameter AFTER calling init simulator method."
        self._seedArrive = seed

    def setSeedDst(self, seed):
        if (self._initReady):
            raise "You can not set seed destiny parameter AFTER calling init simulator method."
        self._seedArrive = seed

    def setGoalConnections(self, goal):
        if (self._initReady):
            raise "You can not set goal connections parameter AFTER calling init simulator method."
        self._goalConnections = goal

    def setBitRates(self, bitRates):
        if (self._initReady):
            raise "You can not set bitrates parameter AFTER calling init simulator method"
        self._bitRatesDefault = bitRates

    def setAllocator(self, algorithm):
        # Revisar codigo antiguo
        # if (self._initReady):
        #     raise "You can not set allocator parameter AFTER calling init simulator method."
        # allocator.setNetwork(self._controller._network)
        # allocator.setPath(self._controller.getPaths())
        # self._controller.setAllocator(allocator)
        # Fin revisar
        self._controller._allocator = algorithm

    def setConfidence(self, c):
        if (c <= 0 or c >= 1):
            raise "You can't set a confidence interval with confidence equal/higher than 1 or equal/lower than 0."
        self._confidence = c

    def printInitialInfo(self):
        print("Nodes: ", end='')
        print(self._controller._network.getNumberOfNodes())
        print("Links: ", end='')
        print(self._controller._network.getNumberOfLinks())
        print("Goal Connections: ", end='')
        print(self._goalConnections)
        print("Lambda: ", end='')
        print(self._lambda)
        print("Mu: ", end='')
        print(self._mu)
        # TO DO
        # print("Algoritmo: ", end='')
        # print(self._controller._allocator.getName())
        print('+', end='')
        for i in range(0, 7):
            print('{:->11}'.format('+'), end='')
        print("\n|", end='')
        print('{: >11}'.format('progress |'), end='')
        print('{: >11}'.format('arrives |'), end='')
        print('{: >11}'.format('blocking |'), end='')
        print('{: >11}'.format('time(s) |'), end='')
        print('{: >11}'.format('Wald CI |'), end='')
        print('{: >11}'.format('A-C. CI |'), end='')
        print('{: >11}'.format('Wilson CI |'))
        print('+', end='')
        for i in range(0, 7):
            print('{:->11}'.format('+'), end='')
        print("")
        self._startingTime = datetime.now()

    def printRow(self, percentage):
        self._checkTime = datetime.now()
        self._timeDuration = self._checkTime - self._startingTime
        print('|', end='')
        print('{: >11}'.format(str(percentage)+'% |'), end='')
        print('{: >11}'.format(str(self._numberOfConnections - 1)+' |'), end='')
        print('{: >11}'.format("{0:.6f}".format(self.getBlockingProbability())+' |'), end='')
        print('{: >11}'.format(str(self._timeDuration).split(".")[0]+' |'), end='')
        print('{: >11}'.format("{0:.6f}".format(self.waldCI())+' |'), end='')
        print('{: >11}'.format("{0:.6f}".format(self.agrestiCI())+' |'), end='')
        print('{: >11}'.format("{0:.6f}".format(self.wilsonCI())+' |'))
        print('+', end='')
        for i in range(0, 7):
            print('{:->11}'.format('+'), end='')
        print("")

    def eventRoutine(self, action):
        self._currentEvent = self._events[0]
        self._rtnAllocation = Controller.Status.N_A
        self._clock = self._currentEvent.getTime()
        if (self._currentEvent.getType().name == EventType.Arrive.name):
            nextEventTime = self._clock + self._arriveVariable.getNextValue()
            for pos in range(len(self._events)-1, -1, -1):
                if (self._events[pos].getTime() < nextEventTime):
                    self._numberOfConnections += 1
                    self._events.insert(pos+1, Event(
                        EventType.Arrive, nextEventTime, self._numberOfConnections))
                    break
            self._src = self._srcVariable.getNextIntValue()
            self._dst = self._dstVariable.getNextIntValue()

            while (self._src == self._dst):
                self._dst = self._dstVariable.getNextIntValue()
            self._bitRate = self._bitRateVariable.getNextIntValue()

            # Se redondea porque no se puede buscar un flotante en un arreglo
            self._bitRate = round(self._bitRate)

            self._rtnAllocation = self._controller.assignConnection(
                self._src, self._dst, self._bitRates[self._bitRate], self._currentEvent.getIdConnection(), action)
            if (self._rtnAllocation.name == Controller.Status.Allocated.name):
                nextEventTime = self._clock + self._departVariable.getNextValue()
                for pos in range(len(self._events)-1, -1, -1):
                    if (self._events[pos].getTime() < nextEventTime):
                        self._events.insert(pos+1, Event(
                            EventType.Departure, nextEventTime, self._currentEvent.getIdConnection()))
                        break
                self._allocatedConnections += 1
        else:
            if (self._currentEvent.getType().name == EventType.Departure.name):
                self._controller.unassignConnection(
                    self._currentEvent.getIdConnection())
        self._events.pop(0)
        self.__lastConnectionIsAllocated = self._rtnAllocation
        return self._rtnAllocation

    def init(self):
        self._initReady = True
        self._clock = 0
        self._arriveVariable = ExpVariable(
            self._seedArrive, self._lambda)
        self._departVariable = ExpVariable(
            self._seedDeparture, self._mu)
        self._srcVariable = UniformVariable(
            self._seedSrc, self._controller._network.getNumberOfNodes())
        self._dstVariable = UniformVariable(
            self._seedDst, self._controller._network.getNumberOfNodes())
        self._bitRateVariable = UniformVariable(
            self._seedBitRate, len(self._bitRatesDefault))
        # numberOfConnections a 0. Considerar como cantidad de conexiones previas al evento
        self._numberOfConnections = 0
        self._events.append(Event(
            EventType.Arrive, self._arriveVariable.getNextValue(), self._numberOfConnections))
        self._bitRates = self._bitRatesDefault
        self.initZScore()

    def run(self):
        timesToShow = 20 #Aqui se modifica la frecuencia del muestreo de datos
        arrivesByCicle = self._goalConnections / timesToShow
        self.printInitialInfo()
        for i in range(1, timesToShow):
            while(self._numberOfConnections <= (i * arrivesByCicle)):
                self._currentEvent = self._events[0]
                self.eventRoutine(None)
            self.printRow((100 / timesToShow) * i)
    
    def step(self, action):
        self._currentEvent = self._events[0]
        if (self._currentEvent.getType() == EventType.Arrive):
            self.eventRoutine(action)
        else:
            while True:
                self.eventRoutine(None)
                self._currentEvent = self._events[0]
                if (self._currentEvent.getType() == EventType.Arrive):
                    break
            self.eventRoutine(action)
                


    def getTimeDuration(self):
        return self._timeDuration.count()

    def getBlockingProbability(self):
        return 1 - self._allocatedConnections / self._numberOfConnections

    def getAllocatedProbability(self):
        return self._allocatedConnections / self._numberOfConnections

    def waldCI(self):
        np = self.getAllocatedProbability()
        p = 1 - np
        n = self._numberOfConnections
        sd = math.sqrt((np * p) / n)
        return self._zScore * sd

    def agrestiCI(self):
        np = self.getAllocatedProbability()
        n = self._numberOfConnections
        if self._allocatedConnections != 0:
            np = np * ((n * (self._allocatedConnections + 2)) /
                   (self._allocatedConnections * (n + 4)))
        p = 1 - np
        sd = math.sqrt((np * p) / (n + 4))
        return self._zScore * sd

    def wilsonCI(self):
        np = self.getAllocatedProbability()
        p = 1 - np
        n = self._numberOfConnections
        denom = (1 + (math.pow(self._zScore, 2) / n))
        k = p + math.pow(self._zScore, 2) / (2 * n)
        sd = math.sqrt(((np * p) / n) +
                       ((math.pow(self._zScore, 2)) / (4 * math.pow(n, 2))))
        return (self._zScore * sd) / denom

    def initZScore(self):
        actual = 0.0
        step = 1.0
        covered = 0.0
        objetive = self._confidence
    #     # con 1e-2 funciona, no es la idea por ningun motivo pero sirve para probar por mientras (1e-6)
        epsilon = 1e-2
        while (math.fabs(objetive - covered) > epsilon):

            meta = math.fabs(objetive - covered)

            print("meta: {}".format(meta))
            print("objetivo: {} covered: {} epsilon: {}".format(
                 objetive, covered, epsilon))

            if(objetive > covered):
                actual += step
                covered = ((1 + math.erf(actual / math.sqrt(2))) -
                           (1 + math.erf(-actual / math.sqrt(2)))) / 2
                if (covered > objetive):
                    step /= 2
            else:
                actual -= step
                convered = ((1 + math.erf(actual / math.sqrt(2))) -
                            (1 + math.erf(-actual / math.sqrt(2)))) / 2
                if (convered < objetive):
                    step /= 2
        self._zScore = actual

    def defaultValues(self):
        self._initReady = False
        self._lambda = 1000
        self._mu = 100
        self._seedArrive = 12345
        self._seedDeparture = 12345
        self._seedSrc = 12345
        self._seedDst = 12345
        self._seedBitRate = 12345
        self._numberOfConnections = 0
        self._numberOfEvents = 0
        self._goalConnections = 10000
        self._columnWidth = 10
        self._confidence = 0.95
        self._allocatedConnections = 0
        
    def lastConnectionIsAllocated(self):
        return self.__lastConnectionIsAllocated
    
    def setAgentAction(self, action):
        self.__agentAction = action
        
    def getDoneFunc(self):
        return self.__doneFunc
