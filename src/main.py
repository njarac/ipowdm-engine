from IPoWDM import IPoWDM
import os

absolutePath = os.path.abspath(__file__)
fileDirectory = os.path.dirname(absolutePath)

wdm = IPoWDM(fileDirectory + "/UKNet_Netgraph20.json", fileDirectory + "/UKNet_baroni.json", 1000, 0.05)

wdm.calculateElasticProbability()
wdm.calculateElasticProbabilityUpStream()