# -*- coding: utf-8 -*-
"""
Created on Tue May 17 12:41:23 2022

@author: redno
"""
from randomVariable import RandomVariable
import math


class ExpVariable:

    def __init__(self, seed=1234567, parameter1=10):
        if (parameter1 <= 0):
            raise("Lambda parameter must be positive.")
        self.rn = RandomVariable(seed, parameter1)

    # dist corresponde a un objeto en c++
    def getNextValue(self):
        return -1*(math.log(1 - self.rn.getDist(self.rn._generator)) / self.rn._parameter1)
