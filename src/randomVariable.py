# -*- coding: utf-8 -*-
"""
Created on Tue May 17 13:32:05 2022

@author: redno
"""
from numpy.random import Generator, MT19937
import numpy as np


class RandomVariable:

    def __init__(self, seed=1234567, parameter1=10):
        self._generator = Generator(MT19937(seed))
        self._parameter1 = parameter1
        self._dist = self._generator.uniform(0, 1.0)

    def getDist(self, generator):
        self._dist = generator.uniform(0, 1.0)
        return self._dist
