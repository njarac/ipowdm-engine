# -*- coding: utf-8 -*-
"""
Created on Tue Feb 22 16:08:34 2022

@author: redno
"""
import json

class BitRate:
    _bitRate = 0.0
    _modulation = None
    _slots = None
    _reach = None
    
    def __init__(self, bitRate=None):
        self._bitRate = bitRate
        self._modulation = []
        self._slots = []
        self._reach = []
    
    def addModulation(self, modulation, slots, reach):
        self._modulation.append(modulation)
        self._slots.append(slots)
        self._reach.append(reach)
    
    def getModulation(self, position):
        if (position >= len(self._modulation)):
            raise ("Bitrate "+self._bitRate+" does not have more than "+len(self._modulation)+" modulations.")
        return self._modulation[position]
    
    def getNumberofSlots(self, position):
        if (position >= len(self._slots)):
            raise("Bitrate "+self._bitRate+" does not have more than "+len(self._slots)+" slots.")
        return self._slots[position]
    
    def getReach(self, position):
        if (position >= len(self._reach)):
            raise ("Bitrate "+self._bitRate+" does not have more than "+len(self._reach)+" reach.")
        return self._reach[position]
    
    def readBitRateFile(self, fileName):
        with open(fileName) as json_file:
            info = json.load(json_file)
            #json_strings = json.dumps(data, indent=4)
            bitsRate = []
            for tag in info:
                bitRate = BitRate(tag)
                for name in info[tag]:
                    for modulation in name:
                        bitRate.addModulation(modulation,name[modulation]['slots'],name[modulation]['reach'])
                bitsRate.append(bitRate)
            return bitsRate
