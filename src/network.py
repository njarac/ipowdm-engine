# -*- coding: utf-8 -*-
"""
Created on Mon Jan 24 17:51:19 2022

@author: redno
"""
from readerJson import Reader
import sys
sys.path.append('../')


class Network:
    _linkCounter = 0
    _nodeCounter = 0
    _nodes = None
    _links = None
    _linksIn = None
    _linksOut = None
    _nodesIn = None
    _nodesOut = None

    def __init__(self, filename, timeOn, timeOff):
        self._linkCounter = 0
        self._nodeCounter = 0
        self._nodes = []
        self._links = []
        self._linksIn = []
        self._linksOut = []
        self._nodesIn = []
        self._nodesOut = []
        self._nodesIn.append(0)
        self._nodesOut.append(0)

        # Open JSON File
        j = Reader()
        j.readNetwork(filename, self._nodes, self._links)
        # Number of Nodes
        self._nodeCounter = len(self._nodes)
        self._linkCounter = len(self._links)
        outCount = 0
        inCount = 0
        for i in range(self._nodeCounter):
            for j in range(self._linkCounter):
                if (i == self._links[j]._src):
                    self._linksOut.append(self._links[j])
                    outCount = outCount + 1
                if (i == self._links[j]._dst):
                    self._linksIn.append(self._links[j])
                    inCount = inCount + 1
            self._nodesOut.append(outCount)
            self._nodesIn.append(inCount)

    def addNode(self, node):
        if (node._id != self._nodeCounter):
            raise("Cannot add a Node to this network with Id mismatching node counter.")
        self._nodeCounter = self._nodeCounter + 1
        self._nodes.append(node)
        self._nodesIn.append(0)
        self._nodesOut.append(0)

    def addLink(self, link):
        if (link._id != self._linkCounter):
            raise("Cannot add a Link to this network with Id mismatching link counter.")
        self._linkCounter = self._linkCounter + 1
        self._links.append(link)

    def connect(self, src, linkPos, dst):
        if (src < 0 or src >= self._nodeCounter):
            raise("Cannot connect src "+src +
                  " because its ID is not in the network. Number of nodes in network: "+self._nodeCounter)
        if (dst < 0 or dst >= self._nodeCounter):
            raise("Cannot connect dst "+dst +
                  " because its ID is not in the network. Number of nodes in network: "+self._nodeCounter)
        if (linkPos < 0 or linkPos >= self._linkCounter):
            raise("Cannot use link "+linkPos +
                  " because its ID is not in the network. Number of links in network: "+self._linkCounter)
        self._linksOut.insert(
            self._linksOut[0] + self._nodesOut[src], self._links[linkPos])
        for n in range(self._nodesOut[0] + src + 1, self._nodesOut[-1]):
            self._nodesOut[n] = self._nodesOut[n] + 1
        self._linksIn.insert(
            self._linksIn[0] + self._nodesIn[dst], self._links[linkPos])
        for n in range(self._nodesIn[0] + dst + 1, self._nodesIn[-1]):
            self._nodesIn[n] = self._nodesIn[n] + 1
        self._links[linkPos]._src = src
        self._links[linkPos]._dst = dst

    def isConnected(self, src, dst):
        for i in range(self._nodesOut[src], self._nodesOut[src+1]):
            for j in range(self._nodesIn[dst], self._nodesIn[dst+1]):
                if(self._linksOut[i]._id == self._linksIn[j]._id):
                    return self._linksOut[i]._id
        return -1

    def useSlot(self, linkPos, slotFrom, slotTo = None):
        if (slotTo is None):
            if (linkPos < 0 or linkPos > self._linkCounter):
                raise("Link position out of bounds.")
            if (self._links[linkPos]._slots[slotFrom] == True):
                raise("Bad assignation on slot",slotFrom)
            self._links[linkPos]._slots[slotFrom] = True
        else:
            self.validateSlotFromTo(linkPos, slotFrom, slotTo)
            for i in range(slotFrom, slotTo):
                self._links[linkPos]._slots[i] = True

    def unuseSlot(self, linkPos, slotFrom, slotTo = None):
        if (slotTo is None):
            if (linkPos < 0 or linkPos > self._linkCounter):
                raise("Link position out of bounds.")
            self._links[linkPos]._slots[slotFrom] = False
        else:
            self.validateSlotFromTo(linkPos, slotFrom, slotTo)
            for i in range(slotFrom, slotTo):
                self._links[linkPos]._slots[i] = False

    def isSlotUsed(self, linkPos, slotPos):
        if (linkPos < 0 or linkPos >= self.linksCounter):
            raise("Link position out of bounds.")
        if (slotPos < 0 or slotPos >= self.links[linkPos].getSlots()):
            raise("slot position out of bounds.")
        return self._links[linkPos]._slots[slotPos]

    def validateSlotFromTo(self, linkPos, slotFrom, slotTo):
        if (linkPos < 0 or linkPos >= self._linkCounter):
            raise("Link position out of bounds.")
        if (slotFrom < 0 or slotFrom >= self._links[linkPos].getSlots()):
            raise("slot position out of bounds.")
        if (slotTo < 0 or slotTo > self._links[linkPos].getSlots()):
            raise("slot position out of bounds.")
        if (slotFrom > slotTo):
            raise("Initial slot position must be lower than the final slot position.")
        if (slotFrom == slotTo):
            raise("Slot from and slot To cannot be equals.")

    def getNumberOfNodes(self):
        return self._nodeCounter

    def getNumberOfLinks(self):
        return self._linkCounter

    def getLink(self, idLink):
        return self._links[idLink]
