# -*- coding: utf-8 -*-
"""
Created on Tue Apr 26 15:29:56 2022

@author: redno
"""
# from .allocator import Allocator
import string

from network import Network
from connection import Connection
import enum
import json


class Controller:

    def __init__(self, network=None):
        self._connections = []
        self._allocator = None
        self._network = network
        self._path = []

    
    Status = enum.Enum('Status','Allocated Not_Allocated N_A')

    # @property
    # def allocator(self):
    #     return self._allocator

    # @allocator.setter
    # def allocator(self, allocator):
    #     self._allocator = allocator

    def assignConnection(self, src, dst, bitRate, idConnection, action = None):
        con = Connection(idConnection)

        # Problema inicial, faltaban dos argumentos network y path, la solucion torpe es agregarlos pero no usarlos
        # actualización estas variables deben entrar
        rtnAllocation, con = self._allocator(
            src, dst, bitRate, con, self._network, path=self._path, action=action)
        if (rtnAllocation.name == Controller.Status.Allocated.name):
            self._connections.append(con)
            for j in range(0, len(con._links)):
                self._network.useSlot(con._links[j]._id, con._slots[j][0], con._slots[j][0]+len(con._slots[j]))

        return rtnAllocation

    def unassignConnection(self, idConnection):
        for i in range(0, len(self._connections)):
            if (self._connections[i]._id == idConnection):
                con = self._connections[i]
                for j in range(0, len(con._links)):
                    self._network.unuseSlot(con._links[j]._id, con._slots[j][0], con._slots[j][0]+len(con._slots[j]))
                
                self._connections.pop(i)
                break

        return 0

    def setPaths(self, filename, timeOn, timeOff):
        with open(filename) as json_file:
            filePaths = json.load(json_file)
        numberOfNodes = self._network.getNumberOfNodes()
        self._path = []

        for i in range(0, numberOfNodes):
            self._path.append([])
            for j in range(0, numberOfNodes):
                self._path[i].append([])
        routesNumber = len(filePaths['routes'])

        for i in range(0, routesNumber):
            # Numero de caminos
            #     "paths": [
            #     [0, 1],
            #     [0, 2, 1],
            #     [0, 7, 6, 4, 3, 1]
            #   ]
            pathsNumber = len(filePaths['routes'][i]['paths'])

            # "src": 0,
            src = filePaths['routes'][i]['src']

            # "dst": 1,
            dst = filePaths['routes'][i]['dst']
            if(isinstance(filePaths['routes'][i]['paths'][0],int)):
                self._path[src][dst].append([])
                nodesPathNumber = len(filePaths['routes'][i]['paths'])
                lastNode = nodesPathNumber - 1
                connection = Connection(i, timeOn = timeOn, timeOff = timeOff)
                for k in range(0, lastNode):
                    actNode = filePaths['routes'][i]['paths'][k]
                    nextNode = filePaths['routes'][i]['paths'][k+1]
                    idLink = self._network.isConnected(actNode, nextNode)
                    self._path[src][dst][0].append(
                        self._network.getLink(idLink))
                    #Crea conecciones y el enlace conoce todas las conexiones que pasan por él.
                    self._network.getLink(idLink).getConnections().append(connection)
                    connection.getLinks().append(self._network.getLink(idLink))
                connection.setBandwidth(1 + ((connection.getSource()+connection.getDestiny()) % 3))
                self._connections.append(connection)
            else:
                for j in range(pathsNumber):
                    self._path[src][dst].append([])

                for j in range(pathsNumber):
                    # ubicación de un camino
                    #   "paths": [
                    #     [0, 1],      <---- este por ejemplo
                    #     [0, 2, 1],
                    #     [0, 7, 6, 4, 3, 1]
                    #   ]
                    nodesPathNumber = len(filePaths['routes'][i]['paths'][j])
                    lastNode = nodesPathNumber - 1
                    connection = Connection(i, timeOn = timeOn, timeOff = timeOff)

                        
                    for k in range(0, lastNode):
                        actNode = filePaths['routes'][i]['paths'][j][k]
                        nextNode = filePaths['routes'][i]['paths'][j][k+1]
                        idLink = self._network.isConnected(actNode, nextNode)
                        self._path[src][dst][j].append(
                            self._network.getLink(idLink))
                        
                        #Crea conecciones y el enlace conoce todas las conexiones que pasan por él.
                        if (j == 0):
                            self._network.getLink(idLink).getConnections().append(connection)
                            connection.getLinks().append(self._network.getLink(idLink))
                    if (j == 0):
                        connection.setBandwidth(1 + ((connection.getSource()+connection.getDestiny()) % 3))
                        self._connections.append(connection)
        return 0
