from IPoWDM import IPoWDM
import os

absolutePath = os.path.abspath(__file__)
fileDirectory = os.path.dirname(absolutePath)


rho = 0.45
quality = 0.001
print("Bienvenido")

#######################################################################################
#Algoritmo básico recibe un slot máximo y va bajando hasta que alguna conexión no cumpla con 
#la calidad de servicio, una vez que ocurre, se termina el algoritmo y se posiciona la cantidad
#de slots superior.
#######################################################################################

#slotValue = 150 #MAX US #49 MAX UK
#while True:
#    wdm = IPoWDM(fileDirectory + "/UKNet_Netgraph16.json", fileDirectory + "/UKNet_baroni.json", 1000, rho)
#    wdm.changeNetworkSlots(slotValue)
#    blocking = wdm.calculateElasticProbabilityUpStream()
#    if (not wdm.evaluateQualityOfNetwork(quality)):
#        slotValue = slotValue + 1
#        break
#    slotValue = slotValue - 1


#wdm = IPoWDM(fileDirectory + "/UKNet_Netgraph16.json", fileDirectory + "/UKNet_baroni.json", 1000, rho)
#wdm.changeNetworkSlots(slotValue)
#blocking = wdm.calculateElasticProbabilityUpStream()
#print("Resultado final: Dimensionamiento: ",slotValue, ", Probabilidad: ",blocking)

#######################################################################################
#Algoritmo logarítmico recibe un slot máximo y mínimo, entre ese rango encuentra la cantidad de slot suficientes
#para cumplir con la calidad de servicio, básicamente divide su rango de slots máximo y mínimo hasta encontrar
#el slots superior. Este arbol log en base 2, comente ciertos errores en el arbol de decisiones, dado que puede 
#en un nivel superior (antes de llegar a las hojas), moverse a un subconjunto donde todo no cumpla la calidad
#de servicio. Es por eso que en las pruebas, al final se suman 2 slots a todos los enlances para asegurar la calidad
#de servicio.
#######################################################################################

slotMin = 0
slotMax = 70 #MAX 50 UK
slotTemp = int((slotMin+slotMax)/2)

while True:
    wdm = IPoWDM(fileDirectory + "/NSFNet.json", fileDirectory + "/NSFNet_routes.json", 1000, rho)
    wdm.changeNetworkSlots(slotTemp)
    blocking = wdm.calculateElasticProbabilityUpStream()
    if (not wdm.evaluateQualityOfNetwork(quality)):
        slotMin = slotTemp
    else:
        slotMax = slotTemp
    slotTemp = int((slotMin+slotMax)/2)
    if (abs(slotMax-slotTemp) <= 1 or abs(slotMin-slotTemp) <= 1):
        break


wdm = IPoWDM(fileDirectory + "/NSFNet.json", fileDirectory + "/NSFNet_routes.json", 1000, rho)
wdm.changeNetworkSlots(slotTemp+2)
blocking = wdm.calculateElasticProbabilityUpStream()
print("Resultado final: Dimensionamiento: ",slotTemp+2, ", Probabilidad: ",blocking)

#######################################################################################
#Algoritmo que busca el detalle y reducir al mínimo la cantidad de slots por enlace que necesita la red por cada enlace.
#Para ello busca la conexión con menos probabilidad de bloqueo se almacena y se le resta en uno a todos los enlaces involucrados.
#Luego se pone a prueba si toda la red cumple la calidad de servicio, si la cumple, se sigue iterando, si no, se devuelve a su estado
#anterior y se deja esa conexión bloqueda para que nunca más sea considerada para bajar más sus slots.
#El algoritmo termina cuando todos las conexiones quedan bloquedas con sus slots.
#######################################################################################
while True:
    #Parte que mejora las conecciones
    connection, blocking = wdm.getLowerConnection()
    if (connection == None):
        break
    #print("Nodo: ",connection.getSource()," a ",connection.getDestiny()," con bloqueo ",blocking)

    for i in range(connection.getLinksLength()):
        linkId = connection.getLink(i)
        link = wdm.getNetwork().getLink(linkId._id)
        link.setSlots(link.getSlots()-1)
        #print("Bajando enlace: ",linkId._id," de ",link.getSlots()+1," a ",link.getSlots())

    oldWdm = wdm
    wdm = IPoWDM(fileDirectory + "/NSFNet.json", fileDirectory + "/NSFNet_routes.json", 1000, rho)
    wdm.changeNetworkSlotsByLink(oldWdm.getArraySlotsByLink())
    blocking = wdm.calculateElasticProbabilityUpStream()
    wdm.changeNetworkConnectionStatus(oldWdm.getConnectionStatus())
    #print("Resultado final: Dimensionamiento, nueva probabilidad: ",blocking)

    if (not wdm.evaluateQualityOfNetwork(quality)):
        connection = wdm.getConnection(connection.getSource(),connection.getDestiny())
        for i in range(connection.getLinksLength()):
            linkId = connection.getLink(i)
            link = wdm.getNetwork().getLink(linkId._id)
            link.setSlots(link.getSlots()+1)
        connection._checked = True
        #print("Fijada conexión, Nodo: ",connection.getSource()," a ",connection.getDestiny()," con bloqueo ",connection._probability)
    #else:
        #print("Puede seguir bajando")
print (wdm.getArraySlotsByLink())
